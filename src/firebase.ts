import { initializeApp } from 'firebase/app'
import { getAuth, connectAuthEmulator } from 'firebase/auth'
import { getFirestore, connectFirestoreEmulator } from 'firebase/firestore'
import firebase from 'firebase/app';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyB1qzy4X1UJ9lznbxEfHHVBGN6s_L8yJ9U',
  authDomain: 'tally-c24da.firebaseapp.com',
  projectId: 'tally-c24da',
  storageBucket: 'tally-c24da.appspot.com',
  messagingSenderId: '369095533413',
  appId: '1:369095533413:web:ff873b98ed81e6cc98378c',
  measurementId: 'G-6WF3TE4Y4J',
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
if (window.location.hostname === 'localhost') {
  connectAuthEmulator(getAuth(), 'http://localhost:9099')
  connectFirestoreEmulator(getFirestore(), 'localhost', 8080)
}

export default app
