import { Menu } from 'antd'
import {
  AreaChartOutlined,
  InfoCircleOutlined,
  LogoutOutlined,
  SettingOutlined,
  UpOutlined,
  UserOutlined,
} from '@ant-design/icons'
import FirebaseHandler from '../FirebaseHandler'
import React from 'react'
import Pages from '../Models/Pages'

const Navbar = (props: props) => {
  const { SubMenu } = Menu
  const firebaseHandler = FirebaseHandler.getFirebaseHandler()

  const signOutUser = async () => {
    await firebaseHandler.signOut()
  }

  return (
    <div className={'nav'}>
      <Menu
        mode={'horizontal'}
        theme={'light'}
        defaultSelectedKeys={[props.defaultKey]}
      >
        <Menu.Item
          key={Pages.Tally}
          icon={<UpOutlined />}
          onClick={() => props.setPage(Pages.Tally)}
        >
          Current Tally
        </Menu.Item>
        <Menu.Item
          key={Pages.Ranks}
          icon={<AreaChartOutlined />}
          onClick={() => props.setPage(Pages.Ranks)}
        >
          High Score
        </Menu.Item>
        <Menu.Item
          eventKey={Pages.About}
          icon={<InfoCircleOutlined />}
          onClick={() => props.setPage(Pages.About)}
        >
          About
        </Menu.Item>
        <SubMenu
          key="sub"
          title={'Settings'}
          icon={<SettingOutlined />}
          style={{ marginLeft: 'auto' }}
        >
          <Menu.Item
            key={Pages.Account}
            icon={<UserOutlined />}
            onClick={() => props.setPage(Pages.Account)}
          >
            My Account
          </Menu.Item>
          <Menu.Item icon={<LogoutOutlined />} onClick={signOutUser}>
            Sign Out
          </Menu.Item>
        </SubMenu>
      </Menu>
    </div>
  )
}

interface props {
  setPage: Function
  defaultKey: Pages
}

export default Navbar
