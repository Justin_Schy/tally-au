import { Typography, Image, Space, Card } from 'antd'
import SignInForm from './SignInForm'
import React, { useState } from 'react'
import SignUpForm from './SignUpForm'

const Auth = (props: props) => {
  const [activeTab, setActiveTab] = useState('signin')

  const tabList = [
    {
      key: 'signin',
      tab: 'Sign In',
    },
    {
      key: 'signup',
      tab: 'Sign Up',
    },
  ]

  const contentList = {
    signin: <SignInForm setLoggedIn={props.setLoggedIn} />,
    signup: <SignUpForm />,
  }

  const title = (
    <div>
      <Space direction={'vertical'} size={50} className={'whole-width'}>
        <span className={'tally-top'}>
          <Image width={40} preview={false} src={'/images/tally.png'} />
          <Typography.Text strong={true} className={'tally-name'}>
            TallyAu
          </Typography.Text>
        </span>
        <div>
          <Typography.Title level={2}>Welcome Back!</Typography.Title>
          <Typography.Text>Please Sign In to Continue</Typography.Text>
        </div>
      </Space>
    </div>
  )

  return (
    <div className={'signIn'}>
      <Card
        title={title}
        defaultActiveTabKey={activeTab}
        tabList={tabList}
        activeTabKey={activeTab}
        onTabChange={key => {
          setActiveTab(key)
        }}
      >
        <React.Fragment>
          {activeTab === 'signin' && contentList['signin']}
          {activeTab === 'signup' && contentList['signup']}
        </React.Fragment>
      </Card>
    </div>
  )
}

interface props {
  setLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
}

export default Auth
