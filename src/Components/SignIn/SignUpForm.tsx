import { Button, Form, Input } from 'antd'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import React, { useState } from 'react'
import FirebaseHandler from '../../FirebaseHandler'

const SignUpForm = () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [repass, setRepass] = useState('')
  const [loading, setLoading] = useState(false)

  const firebaseHandler = FirebaseHandler.getFirebaseHandler()

  const onFinish = () => {
    setLoading(true)
    firebaseHandler.signUp(username, password)
  }

  return (
    <Form
      name="sign_up"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          { required: true, message: 'Please input your Email!' },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (
                !value ||
                getFieldValue('email').indexOf('anderson.edu') !== -1
              ) {
                return Promise.resolve()
              }
              return Promise.reject(
                new Error('Please enter an anderson email!')
              )
            },
          }),
        ]}
      >
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder="Email"
          autoFocus
          onChange={e => setUsername(e.target.value)}
          size={'large'}
        />
      </Form.Item>

      <Form.Item
        name="password"
        rules={[{ required: true, message: 'Please input your Password!' }]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          placeholder="Password"
          onChange={e => setPassword(e.target.value)}
          size={'large'}
        />
      </Form.Item>

      <Form.Item
        name="re-password"
        rules={[
          { required: true, message: 'Please input your Password!' },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve()
              }
              return Promise.reject(
                new Error('The two passwords that you entered do not match!')
              )
            },
          }),
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          placeholder="Password"
          onChange={e => setRepass(e.target.value)}
          size={'large'}
        />
      </Form.Item>

      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          disabled={
            username === '' ||
            password === '' ||
            repass === '' ||
            repass !== password
          }
          loading={loading}
          className="form-submit primary"
        >
          Sign Up!
        </Button>
      </Form.Item>
    </Form>
  )
}

export default SignUpForm
