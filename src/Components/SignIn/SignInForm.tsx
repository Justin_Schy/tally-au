import { Button, Checkbox, Form, Input } from 'antd'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import FirebaseHandler from '../../FirebaseHandler'
import React, { useState } from 'react'

const SignInForm = (props: props) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [persist, setPersistence] = useState(false)
  const [loading, setLoading] = useState(false)

  const firebaseHandler = FirebaseHandler.getFirebaseHandler()

  const signedIn = (verified: boolean) => {
    setLoading(false)
    if (verified) {
      props.setLoggedIn(true)
    }
  }

  const signInFailure = () => {
    setLoading(false)
  }

  const onFinish = () => {
    firebaseHandler.signIn(username, password, persist, signedIn, signInFailure)
  }

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        className={'form-input'}
        rules={[{ required: true, message: 'Please input your Email!' }]}
      >
        <Input
          prefix={<UserOutlined className="site-form-item-icon" />}
          id="username"
          placeholder="Email"
          autoFocus
          onChange={e => setUsername(e.target.value)}
          size={'large'}
        />
      </Form.Item>

      <Form.Item
        name="password"
        rules={[{ required: true, message: 'Please input your Password!' }]}
      >
        <Input
          id="password"
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder="Password"
          onChange={e => setPassword(e.target.value)}
          size={'large'}
        />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName={'checked'}
        className={'form-check'}
      >
        <Checkbox onChange={e => setPersistence(e.target.checked)}>
          Remember me
        </Checkbox>
      </Form.Item>

      <Form.Item>
        <Button
          type="primary"
          id="submit"
          htmlType="submit"
          loading={loading}
          onClick={() => setLoading(true)}
          className="form-submit primary"
        >
          Log in
        </Button>
      </Form.Item>
    </Form>
  )
}

interface props {
  setLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
}

export default SignInForm
