import React, {useEffect, useState} from "react"
import FirebaseHandler from "../../FirebaseHandler"
import Actions, { actionsDataTable, allActions } from "../../Models/Actions"
import { Button, Table } from "antd"
import Pages from "../../Models/Pages"

const ActionsLeft = (props: Props) => {
  const [actions, setActions] = useState<allActions>({div: -1, multi: -1, reset: -1})
  const [data, setData] = useState<Array<actionsDataTable>>([]);
  const firebaseHandler = FirebaseHandler.getFirebaseHandler();


  useEffect(() => {
    if (props.countdown != -1) {
      firebaseHandler.getAllActions().then((actions) => {
        setActions(actions)
        firebaseHandler.getLastUsed(Actions.Divide).then(divLeft => {
          firebaseHandler.getLastUsed(Actions.Multiply).then(multiLeft => {
            firebaseHandler.getLastUsed(Actions.Reset).then(resetLeft => {
              setActionData(actions, multiLeft, divLeft, resetLeft)
            })
          })
        })
      })
    }
  }, [props.countdown])

  const setActionData = (actionsData: allActions, multi: string, div: string, reset: string) => {
    const data = [
      {
        action: "Multiply",
        numLeft: actionsData.multi,
        lastUsed: multi,
        currentCost: `${props.countdown / 4} Points`,
        key: "multiply"
      },
      {
        action: "Divide",
        numLeft: actionsData.div,
        lastUsed: div,
        currentCost: `${props.countdown / 4} Points`,
        key: "divide"
      },
      {
        action: "Reset",
        numLeft: actionsData.reset,
        lastUsed: reset,
        currentCost: `${props.countdown / 2} Points`,
        key: "reset"
      },
    ]
    setData((oldData) => [...data]);
    }

  const columns = [
    {
      title: "Action",
      dataIndex: "action",
      key: "action"
    },
    {
      title: "# Left",
      dataIndex: "numLeft",
      key: "numLeft"
    },
    {
      title: "Last Used",
      dataIndex: "lastUsed",
      key: "lastUsed"
    },
    {
      title: "Action Cost",
      dataIndex: "currentCost",
      key: "currentCost"
    },
  ]


  return (
    <div>
      <div>
        { props.countdown !== -1 && <Table dataSource={data} columns={columns} pagination={false}/> }
      </div>
      <Button className={'bm'} onClick={() => props.setPage(Pages.BlackMarket)}>Black Market</Button>
    </div>

)
}

interface Props {
  countdown: number
  setPage: Function
}

export default ActionsLeft
