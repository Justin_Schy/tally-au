import { Table, Typography } from 'antd'
import FirebaseHandler from '../../FirebaseHandler'
import { useEffect, useState } from 'react'
import RanksType from '../../Models/Ranks'

const Ranks = () => {
  const firebaseHandler = FirebaseHandler.getFirebaseHandler()
  const [data, setData] = useState<Array<RanksType>>([])
  const [error, setError] = useState(false)

  useEffect(() => {
    async function getRanks() {
      const newData = await firebaseHandler.getHighScore()
      if (!newData) {
        setError(true)
      } else {
        setData([...newData])
      }
    }

    getRanks()
  }, [])

  const columns = [
    {
      title: 'Rank',
      dataIndex: 'rank',
      key: 'rank',
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Points',
      dataIndex: 'points',
      key: 'points',
      render: (points: string) => (
        <Typography.Text>{points} Points</Typography.Text>
      ),
    },
  ]

  return (
    <Table
      columns={columns}
      dataSource={data}
      loading={data.length === 0 || error}
      pagination={false}
    />
  )
}

export default Ranks
