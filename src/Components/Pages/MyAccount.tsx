import { Col, Divider, Input, PageHeader, Row, Spin, Typography } from "antd"
import { LoadingOutlined } from '@ant-design/icons';
import { useState, useEffect } from "react";
import FirebaseHandler from "../../FirebaseHandler"

const MyAccount = () => {
  const firebaseHandler = FirebaseHandler.getFirebaseHandler();
  const [name, setName] = useState("");

  useEffect(() => {
    firebaseHandler.getCurrentUserEmail().then((email) => {
      setName(email);
    })
  }, [])


  return (
    <div>
      <div>
        <Input.Group>
        {name === "" && <Spin indicator={<LoadingOutlined/>} />}
          {name !== "" && <Typography.Title level={3}><Row><Col>Welcome </Col><Col><Input className={"inline-input"} bordered={false} value={name}/></Col></Row></Typography.Title>}
        <Divider orientation={"left"}>
          <Typography.Title level={5} type={"secondary"}>Personal Information</Typography.Title>
        </Divider>
        </Input.Group>
      </div>
    </div>
  )
}

export default MyAccount
