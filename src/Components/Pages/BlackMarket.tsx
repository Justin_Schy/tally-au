import { Button, PageHeader, Statistic, Table } from 'antd';
import FirebaseHandler from '../../FirebaseHandler';
import BlackMarketData from '../../Models/BlackMarketData';
import Page from "../../Models/Pages"
import {
  useState
} from 'react'

const BlackMarket = (props: Props) => {
  const [data, setData] = useState<Array<BlackMarketData>> ([])
  const firebaseHandler = FirebaseHandler.getFirebaseHandler();

  firebaseHandler.checkBlackMarket().then((res) => {
    setData([...res])
  })

  const buyItem = (item: string | number) => {
    firebaseHandler.buyBlackMarketItem(item.toString())
  }

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "# Left",
      dataIndex: "numLeft",
      key: "numLeft"
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price"
    },
    {
      title: "Action",
      dataIndex: '',
      key: '',
      render: (_: any, record: {key: React.Key }) => <Button className={'buyButton'} onClick={(e) => {
        buyItem(record.key);
      }}>Buy</Button>
    }
  ]

  return (
    <div>
      <PageHeader
      onBack={() => {props.setPage(Page.Tally)}}
      title={'The Black Market'}
      subTitle={'Buy while its still here!'} />
      <Table dataSource={data} columns={columns} pagination={false}/>
    </div>
  )
}

interface Props {
  setPage: Function
}

export default BlackMarket
