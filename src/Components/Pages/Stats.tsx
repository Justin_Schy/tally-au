import FirebaseHandler from '../../FirebaseHandler'
import {
  DocumentData,
  QueryDocumentSnapshot,
  Timestamp,
} from 'firebase/firestore'
import { useEffect, useState } from 'react'
import TallyData from '../../Models/TallyData'
import ActionsLeft from '../utils/ActionsLeft'
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, TooltipProps, Legend  } from 'recharts';
import {Divider} from 'antd';

const Stats = (props: Props) => {
  const firebaseHandler = FirebaseHandler.getFirebaseHandler()
  const [userProfile, setUserProfile] = useState<DocumentData>({ tallys: [] })
  const [userProfileData, setUserProfileData] = useState<Array<TallyData>>([])

  const updateUserProfile = (
    userProfile: QueryDocumentSnapshot<DocumentData>
  ) => {
    setUserProfile(userProfile)
  }

  useEffect(() => {
    firebaseHandler.getCurrentUserRealtimeUpdates(updateUserProfile)
  }, [firebaseHandler])

  useEffect(() => {
    const data: Array<TallyData> = []
    let totalPoints = 0

    userProfile.tallys.forEach(
      (
        item: {
          Action: string
          Points: number
          When: { seconds: number; nanoseconds: number }
        },
        index: number
      ) => {
        const when = new Timestamp(item.When.seconds, item.When.nanoseconds)
        const whenString = when.toDate().toDateString()
        totalPoints += item.Points
        if (item.Points > 0) {
          data.push({
            date: whenString.substr(4, whenString.length - 8),
            points: item.Points,
            totalPoints: totalPoints,
          })
        }
      }
    )
    setUserProfileData([...data])
  }, [userProfile])

  interface ICustomToolip {
    active: any;
    payload: any;
    label: any;
  }

  const CustomTooltip = ({ active, payload, label }: any) => {
    if (active && payload) {
      return (
        <div className="custom-tooltip">
          <p className="label">{label}</p>
          <p className="desc">Points Earned: <strong className={"right"}>{payload[0].payload?.points}</strong></p>
          <p className="desc">Total Points: <strong className={"right"}>{payload[0].payload?.totalPoints}</strong></p>
        </div>
      );
    }

    return null;
  }

  return (
    <div className="statContainer">
      <div className={"lineGraph"}>
        {userProfileData.length > 0 && <LineChart width={500} height={320} data={userProfileData}>
          <Line type='monotone' dataKey={'totalPoints'} stroke='#8884d8' />
          <CartesianGrid strokeDasharray='3 3' />
          <XAxis dataKey={'date'} />
          <YAxis />
          <Tooltip content={<CustomTooltip />} />
        </LineChart> }
      </div>
      <div className={"numLeft"}>
        <ActionsLeft countdown={props.countdown} setPage={props.setPage}/>
      </div>
    </div>
)
}

interface Props {
  countdown: number
  setPage: Function
}

export default Stats
