import React, { useEffect, useState } from 'react'
import FirebaseHandler from '../../FirebaseHandler'
import Flip from '../Flip'
import { Button, Spin, Tooltip } from 'antd'
import Actions from '../../Models/Actions'
import Countdown from '../Countdown'
import Stats from './Stats'

const Tally = (props: props) => {
  const [count, setCount] = React.useState(0)
  const firebaseHandler = FirebaseHandler.getFirebaseHandler()
  const [divideAvailable, setDivAval] = useState(false)
  const [multiAvaliable, setMultiAval] = useState(false)
  const [resetAvaliable, setResetAvaliable] = useState(false)

  useEffect(() => {
    async function checkActions() {
      setDivAval(await firebaseHandler.divideAvailable())
      setMultiAval(await firebaseHandler.multiAvaliable())
      setResetAvaliable(await firebaseHandler.resetAvaliable())
    }

    checkActions()
  }, [setDivAval, firebaseHandler, count])

  /**
   * Update tally by some amount
   * @param By the amount to update
   * @param action thr action updating the tally
   */
  function updateTally(By: number, action: Actions) {
    if (action) firebaseHandler.doAction(count, action)
    firebaseHandler.setTally(By, count, action).then(() => {
      props.onClick()
    })
  }

  function halfTally() {
    let half = count / 2
    updateTally(-Math.abs(Math.round(half)), Actions.Divide)
  }

  function doubleTally() {
    let double = count * 2
    updateTally(double - count, Actions.Multiply)
  }

  function resetTally() {
    updateTally(-(count - 1), Actions.Reset);
  }

  useEffect(() => {
    async function getTally() {
      await firebaseHandler.getTally(setCount)
    }

    getTally()
  }, [firebaseHandler])

  let decTitle =
    count <= 1 ? 'Cannot Make Tally Go Below 1' : 'Decrease Tally By One'
  let divideTitle =
    count <= 1 ? 'Cannot Divide when Count is 1' : 'Divide Tally By 2'
  let multiTitle =
    count <= 1 ? 'Cannot Multiply when Count is 1' : 'Multiply Tally By 2'
  let resetTitle =
    count <= 1 ? 'Cannot Reset when Count is 1' : 'Reset Tally to 1'
  let incTitle = 'Increase Tally by 1'

  if (!props.canClick) {
    decTitle = divideTitle = incTitle = multiTitle = resetTitle = 'Cannot Click Currently.'
  }

  if (!divideAvailable) {
    divideTitle = 'No Divides Left :( sorry!'
  }

  if (!multiAvaliable) {
    divideTitle = 'No Multiplys Left :( sorry!'
  }

  if (!resetAvaliable) {
    resetTitle = 'No Resers Left :( sorry!'
  }

  return (
    <div>
      <div id={'counter'} className={count === 0 ? 'loading' : ''}>
        {count !== 0 && <Flip value={count} />}
        {count === 0 && <Spin size="large" />}
      </div>
      <div className={'action-area'}>
        <div className={'action-area-items basic-items'}>
        <Tooltip placement={'top'} title={incTitle}>
          <Button
            type="default"
            id="increase"
            className={"action-button"}
            onClick={() => {
              updateTally(1, Actions.Increment)
            }}
            disabled={!props.canClick}
          >
            Increase by 1
          </Button>
        </Tooltip>
        <Tooltip placement={'top'} title={decTitle}>
          <Button
            type="default"
            id="decrease"
            className={"action-button"}
            onClick={() => {
              updateTally(-1, Actions.Decrement)
            }}
            disabled={count <= 1 || !props.canClick}
          >
            Decrease by 1
          </Button>
        </Tooltip>
        </div>
        <div className={'action-area-items advanced-items'}>
        <Tooltip placement={'bottom'} title={divideTitle}>
          <Button
            type="default"
            id="divide"
            className={"action-button"}
            onClick={halfTally}
            disabled={count <= 1 || !divideAvailable || !props.canClick}
          >
            Divide
          </Button>
        </Tooltip>
        <Tooltip placement={'bottom'} title={multiTitle}>
          <Button
            type="default"
            id="multiply"
            className={"action-button"}
            onClick={doubleTally}
            disabled={count <= 1 || !multiAvaliable || !props.canClick}
          >
            Multiply
          </Button>
        </Tooltip>
        <Tooltip placement={'bottom'} title={resetTitle}>
          <Button
            type="default"
            id="reset"
            className={"action-button"}
            onClick={resetTally}
            disabled={count <= 1 || !resetAvaliable || !props.canClick}
          >
            Reset
          </Button>
        </Tooltip>
        </div>
      </div>
      {!props.canClick && (
        <div className={'counter'}>
          <Countdown />
        </div>
      )}
        <Stats countdown={count} setPage={props.setPage}/>
    </div>
  )
}

interface props {
  canClick: boolean
  onClick: Function
  setPage: Function
}

export default Tally
