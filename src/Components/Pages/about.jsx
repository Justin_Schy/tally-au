import { Collapse, Table, Tag, Typography } from 'antd'

const { Panel } = Collapse

const columns = [
  {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    render: action => (
      <Typography.Title level={5} code strong>
        {action}
      </Typography.Title>
    ),
  },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    render: price => {
      if (price === 'free') {
        return <Tag color={'geekblue'}>{price.toUpperCase()}</Tag>
      } else {
        return <Tag color={'green'}>{price.toUpperCase()}</Tag>
      }
    },
  },
  {
    title: 'Number of Uses',
    dataIndex: 'uses',
    key: 'uses',
    render: uses => <Typography.Text strong>{uses}</Typography.Text>,
  },
  {
    title: 'Effect',
    dataIndex: 'effect',
    key: 'effect',
    render: effect => <Typography.Paragraph>{effect}</Typography.Paragraph>,
  },
]

const data = [
  {
    key: '1',
    action: 'Increase',
    effect: 'Increases The Counter by 1',
    uses: 'Unlimited Uses',
    price: 'free',
  },
  {
    key: '2',
    action: 'Decrease',
    uses: 'Unlimited Uses',
    effect: 'Decreases The Counter by 1',
    price: 'free',
  },
  {
    key: '3',
    action: 'Multiply',
    uses: '5 Uses',
    effect: 'Multiples The Counter by a factor of 2',
    price: '1/4 of The Counter Value',
  },
  {
    key: '4',
    action: 'Divide',
    uses: '5 Uses',
    effect: 'Divides The Counter by a factor of 2',
    price: '1/4 of The Counter value',
  },
  {
    key: '5',
    action: 'Reset',
    uses: '1 Use',
    effect:
      'Resets The Counter to 0',
    price: '1/2 of The Counter Value',
  },
]

const About = () => (
  <div className="about-counter">
    <Collapse defaultActiveKey={['1']} accordion>
      <Panel header="What is tallyAu?" key="1">
        <Typography.Text>
          A game. That is the simple answer. It was created by a CS student who
          was inspired by Reddit's{' '}
          <code>
            <a href="https://www.reddit.com/r/thebutton/">r/TheButton</a>
          </code>
          . This is Anderson University's version of that.{' '}
        </Typography.Text>
        <Typography.Text>
          It will last a total of 2 weeks, unless I decide to add more. The goal
          is to get as many points as possible.{' '}
        </Typography.Text>
        <Typography.Text>
          Each player who signs up with an <code>anderson.edu</code> email, will
          be given one action per day These actions may cost points, and affect
          the current available points.
        </Typography.Text>
        <Typography.Text>
          TallyAu is a very simple game. It is comprised on a single thing.
        </Typography.Text>
        <br />
        <Typography.Text>
          <strong>The Counter.</strong>
        </Typography.Text>
        <br />
        <Typography.Text>
          The Counter controls the game from end to end. It defines the current
          available points.{' '}
        </Typography.Text>
        <Typography.Text>
          Every time you interact with Th Counter, you are give the current
          number of points This is how you win. Get as many points as possible
          in your chicks every day.
        </Typography.Text>
      </Panel>
      <Panel header="How does TallyAu Work?" key="2">
        <Typography.Text>
          Each day you are given one action. These actions very from adding one
          to setting the current tally to 0. This means that you affect other
          players who are attempting to get points. After completing your action
          you will be rewarded with the number of points the counter displayed
          when you pressed it. Sometimes, the counter will be a very low number,
          and others high. The game is all about the stragety about when to cash
          in, and what to do after you do.
        </Typography.Text>
        <br />
        <br />
        <Typography.Text>
          Your total points will be displayed in the <code>My Account</code>{' '}
          tab, and the current high scores will be shown on the{' '}
          <code>High Scores</code> tab. Teamwork is not allowed, and knowing I
          in no way can control this I just ask that you respect the rules and
          have fun.
        </Typography.Text>
      </Panel>
      <Panel header="What actions can be taken?" key="3">
        <Table columns={columns} dataSource={data} pagination={false} />
      </Panel>
    </Collapse>
  </div>
)

export default About
