import { Statistic } from 'antd'

const Countdown = () => {
  const deadlineDate = new Date()
  deadlineDate.setHours(28, 0, 0, 0)

  const deadline = Date.UTC(
    deadlineDate.getFullYear(),
    deadlineDate.getMonth(),
    deadlineDate.getDate(),
    deadlineDate.getHours(),
    deadlineDate.getMinutes(),
    deadlineDate.getSeconds(),
    deadlineDate.getMilliseconds()
  )

  return (
    <Statistic.Countdown
      className={'countdown'}
      title="Time until next tally"
      value={deadline}
    />
  )
}

export default Countdown
