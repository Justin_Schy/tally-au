import FlipNumbers from 'react-flip-numbers'
import React from 'react'

const Flip = props => {
  return (
    <div>
      <div style={{ backgroundColor: '#333333', padding: '10px' }}>
        <FlipNumbers
          height={50}
          width={50}
          color="#fff"
          background={'#333333'}
          play
          numbers={`${props.value}`}
        />
      </div>
    </div>
  )
}

export default Flip
