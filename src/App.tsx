import React, { useEffect, useState } from 'react'
import Auth from './Components/SignIn/Auth'
import { getAuth, onAuthStateChanged } from 'firebase/auth'
import './firebase'
import Tally from './Components/Pages/Tally'
import Navbar from './Components/Navbar'
import FirebaseHandler from './FirebaseHandler'
import Pages from './Models/Pages'
import About from './Components/Pages/about'
import Ranks from './Components/Pages/Ranks'
import BlackMarket from './Components/Pages/BlackMarket'
import FirebaseRequest from './Workflows/FirebaseRequest'
import { Collections } from './Workflows/FirebaseWorkflow'
import MyAccount from './Components/Pages/MyAccount'
import { doc, getDoc, getFirestore } from 'firebase/firestore'

function App() {
  const [loggedIn, setLoggedIn] = useState(getAuth().currentUser !== null)
  const [loading, setLoading] = useState(true)
  const firebaseHandler = FirebaseHandler.getFirebaseHandler()

  const getStartingPage = () => {
    const currentPage = sessionStorage.getItem('current-page')
    let newPage = Pages.Tally
    switch (currentPage) {
      case Pages.About:
        newPage = Pages.About
        break
      case Pages.Ranks:
        newPage = Pages.Ranks
        break
      case Pages.Account:
        newPage = Pages.Account
        break
      case Pages.BlackMarket:
        newPage = Pages.BlackMarket
    }
    return newPage
  }

  const [page, setPage] = useState(getStartingPage())
  const [canClick, setCanClick] = useState(false)

  const changePage = (page: Pages) => {
    sessionStorage.setItem('current-page', page.toString())
    setPage(page)
  }

  const clicked = () => {
    setCanClick(false)
  }

  useEffect(() => {
    onAuthStateChanged(getAuth(), user => {
      if (user) {
        if (user.emailVerified || window.location.hostname === 'localhost') {
          firebaseHandler.checkLastClick().then(res => {
            setCanClick(res)
          })
          setLoggedIn(true)
        } else {
          firebaseHandler.displayNotVerifiedNotification()
          firebaseHandler.signOut()
        }
      } else {
        setLoggedIn(false)
      }
      setLoading(false)
    })
  }, [loggedIn, firebaseHandler])

  return (
    <React.Fragment>
      {!loading && (
        <React.Fragment>
          {!loggedIn && (
            <div className={'tall vertical'}>
              <Auth setLoggedIn={setLoggedIn} />
            </div>
          )}
          {loggedIn && (
            <div>
              {page !== Pages.BlackMarket && <Navbar setPage={changePage} defaultKey={page} />}
              {page === Pages.Tally && (
                <Tally canClick={canClick} onClick={clicked} setPage={setPage} />
              )}
              {page === Pages.About && <About />}
              {page === Pages.Ranks && <Ranks />}
              {page === Pages.Account && <MyAccount/>}
              {page === Pages.BlackMarket && <BlackMarket setPage={setPage}/>}
            </div>
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  )
}
export default App
