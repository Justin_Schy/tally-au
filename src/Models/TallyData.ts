interface TallyData {
  date: string
  points: number
  totalPoints: number
}

export default TallyData
