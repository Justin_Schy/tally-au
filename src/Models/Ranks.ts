interface RanksType {
  email: string
  points: string
  rank: number,
  key: string,
}

export default RanksType
