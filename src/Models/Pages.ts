enum Pages {
  Tally = 'tally',
  About = 'about',
  Ranks = 'ranks',
  Account = 'account',
  BlackMarket = 'black_market'
}

export default Pages
