enum Actions {
  Increment = 'Increment',
  Decrement = 'Decrement',
  Multiply = 'Multi',
  Divide = 'Div',
  Reset = 'Reset',
}

export interface allActions {
  div: number,
  multi: number,
  reset: number
}

export interface actionsDataTable {
  action: string,
  numLeft: number,
  lastUsed: string,
  currentCost: string,
  key?: string,
}

export default Actions
