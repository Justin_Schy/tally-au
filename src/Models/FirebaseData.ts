
interface FirebaseData {
  id: string,
  data: any
}

export default FirebaseData;
