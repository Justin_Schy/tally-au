interface BlackMarketData {
  key: string,
  name: string,
  numLeft: number,
  price: number,
}

export default BlackMarketData
