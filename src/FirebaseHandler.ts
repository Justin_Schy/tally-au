import {
  browserLocalPersistence,
  browserSessionPersistence,
  createUserWithEmailAndPassword,
  getAuth,
  sendEmailVerification,
  setPersistence,
  signInWithEmailAndPassword,
  signOut,
} from 'firebase/auth'
import './firebase'
import {
  collection,
  doc,
  getDoc,
  getDocs,
  getFirestore,
  increment,
  limit,
  onSnapshot,
  orderBy,
  query,
  updateDoc,
  DocumentData,
  arrayUnion,
  Timestamp,
  setDoc,
} from 'firebase/firestore'
import React from 'react'
import { notification } from 'antd'
import Ranks from './Models/Ranks'
import Actions, { allActions } from './Models/Actions'
import BlackMarketData from "./Models/BlackMarketData"
import FirebaseWorkflow, { Collections } from "./Workflows/FirebaseWorkflow"
import FirebaseRequest, { FieldOperation } from './Workflows/FirebaseRequest'

class FirebaseHandler {
  private static firebaseHandler: FirebaseHandler
  private static userProfile: DocumentData
  private firebaseWorkflow: FirebaseWorkflow

  private constructor() {
    this.firebaseWorkflow = new FirebaseWorkflow();
  }

  async getTally(setTally: React.Dispatch<React.SetStateAction<number>>) {
    return onSnapshot(doc(getFirestore(), 'tally', 'tally'), doc => {
      if (doc.exists()) {
        setTally(doc.data().tally)
      }
    })
  }

  async checkLastClick() {
    const userProfile = await this.getCurrentUserProfile()
    if (userProfile) {
      if (userProfile.tallys[userProfile.tallys.length - 1]) {
        const tally = userProfile.tallys[userProfile.tallys.length - 1];
        const date = new Timestamp(
          tally.When.seconds,
          tally.When.nanoseconds
        )
        if (date.toDate().getDate() === Timestamp.now().toDate().getDate()) {
          return false
        } else {
          return true
        }
      }
      return true
    } else {
      return false
    }
  }

  getCurrentUserRealtimeUpdates(onChange: Function) {
    const currentUserId = getAuth().currentUser?.uid
    if (currentUserId) {
      const docRef = doc(getFirestore(), 'users', currentUserId)
      onSnapshot(docRef, profile => {
        if (profile.exists()) {
          onChange(profile.data())
        }
      })
    }
  }

  async getCurrentUserProfile() {
    if (
      !FirebaseHandler.userProfile ||
      FirebaseHandler.userProfile.email !== getAuth().currentUser?.email
    ) {
      const currentUserId = getAuth().currentUser?.uid
      if (currentUserId) {
        const firebaseRequest = new FirebaseRequest()
          .collection(Collections.USERS)
          .doc(currentUserId, 'id');

        const profile = await firebaseRequest.execteRead();

        FirebaseHandler.userProfile = profile[0].data;
      } else {
        return null
      }
    }
    return FirebaseHandler.userProfile
  }

  async getUserProfile(userId: string) {
    const userProfileRequest = new FirebaseRequest()
      .collection(Collections.USERS)
      .doc(userId, "id");
    return await userProfileRequest.execteRead();
  }

  async setTally(
    incrementBy: number,
    pointsToAdd: number,
    action: Actions
  ): Promise<void> {
    await this.addPoints(Math.abs(pointsToAdd), action)
    const firebaseRequest = await new FirebaseRequest()
      .collection(Collections.TALLY)
      .doc("tally", 'id')
      .withPayload({tally: increment(incrementBy)})
      .executeWrite(true)
  }

  signIn(
    username: string,
    password: string,
    persist: boolean,
    signInSuccess: Function,
    signInFailure: Function
  ) {
    const success = (verified: boolean) => {
      signInSuccess(verified)
    }

    const failure = () => {
      signInFailure()
      this.displayBadNotification()
    }

    const auth = getAuth()
    setPersistence(
      auth,
      persist ? browserLocalPersistence : browserSessionPersistence
    )
      .then(() => {
        return signInWithEmailAndPassword(auth, username, password)
          .then(user => success(user.user.emailVerified))
          .catch(() => failure())
      })
      .catch(() => failure())
  }

  displayBadNotification() {
    const message = 'Sign in failed.'
    const desc = 'Email or Password was Incorrect Please try again.'
    this.displayNotification(message, desc)
  }

  displayNotVerifiedNotification() {
    const message = 'Email Unverified!'
    const desc =
      'Please check your email for an email verification and click the link.'
    this.displayNotification(message, desc)
  }

  displayNotification(message: string, description: string) {
    notification.error({
      message: message,
      description: description,
    })
  }

  async signOut(): Promise<boolean> {
    try {
      const auth = getAuth()
      await signOut(auth)
      return true
    } catch (e) {
      return false
    }
  }

  async signUp(email: string, password: string) {
    if (FirebaseHandler.getDomain(email) === '@anderson.edu') {
      createUserWithEmailAndPassword(getAuth(), email, password).then(
        async user => {
          if (window.location.hostname !== 'localhost') {
            sendEmailVerification(user.user)
          }
          const curId = getAuth().currentUser?.uid
          if (curId) {
            await this.createNewUserProfile(email, curId)
          }
          this.signOut()
          notification.success({
            message: 'Created User: ' + email,
            description: 'verify your email in order to sign in!',
          })
        }
      )
    } else {
      notification.error({
        message: 'Email Error',
        description: 'Email was not an anderson email.',
      })
    }
  }

  async getCurrentUserEmail() {
    const userProfile = await this.getCurrentUserProfile();
    return userProfile?.email;
  }

  async getCurrentUserScore() {
    const userProfile = await this.getCurrentUserProfile()
    if (userProfile) {
      return userProfile.score
    }
  }

  async getActionsLeft(action?: Actions) {
    const userProfile = await this.getCurrentUserProfile()
    if (userProfile) {
      switch (action) {
        case Actions.Divide:
          return userProfile?.divLeft
        case Actions.Multiply:
          return userProfile?.multiLeft
        case Actions.Reset:
          return userProfile?.resetLeft
        default:

      }
    }
  }

  async getAllActions(): Promise<allActions> {
    const div = await this.getActionsLeft(Actions.Divide)
    const multi = await this.getActionsLeft(Actions.Multiply)
    const reset = await this.getActionsLeft(Actions.Reset)

    return {div: div, multi: multi, reset: reset}
  }

  async getLastUsed(action: Actions): Promise<string> {
    const userProfile = await this.getCurrentUserProfile()
    let lastUsed = 'Never';

    if (userProfile) {
      const reverse = userProfile.tallys.reverse().filter((filter: any) => filter.Action == action);
      try {
        const date = reverse[0].When
        return (new Timestamp(date.seconds, date.nanoseconds).toDate().toLocaleDateString());
      } catch (e) {
        return lastUsed
      }
      return lastUsed
    } else {
      return "Not Found"
    }
  }


  async divideAvailable() {
    return (await this.getActionsLeft(Actions.Divide)) > 0
  }

  async multiAvaliable() {
    return (await this.getActionsLeft(Actions.Multiply)) > 0
  }

  async resetAvaliable() {
    return (await this.getActionsLeft(Actions.Reset)) > 0
  }

  async addPoints(numPoints: number, action: Actions) {
    const currentUserId = getAuth().currentUser?.uid
    if (currentUserId) {
      const userRef = doc(getFirestore(), 'users', currentUserId)
      const newScore = (await this.getCurrentUserScore()) + numPoints
      const now = Timestamp.now()

      await new FirebaseRequest()
        .collection(Collections.USERS)
        .doc(currentUserId, "id").withPayload({
          score: newScore,
          tallys: arrayUnion({ Points: numPoints, When: now, Action: action})
        }).executeWrite(true);
    }
  }

  async usePoints(numPoints: number, action: Actions) {
    const pointsCost = numPoints / 4
    await this.addPoints(-Math.abs(Math.round(pointsCost)), action)
  }

  async doAction(numPoints: number, action: Actions) {
    if (action !== Actions.Increment && action !== Actions.Decrement) {
      await this.usePoints(numPoints, action)
      await this.useAction(action)
    }
  }

  async useAction(action: Actions) {
    const currentUserId = getAuth().currentUser?.uid
    if (currentUserId) {
      const userRef = doc(getFirestore(), 'users', currentUserId)
      const firebaseRequest = new FirebaseRequest()
        .collection(Collections.USERS)
        .doc(currentUserId, "id")
      switch (action) {
        case Actions.Divide:
          firebaseRequest.withPayload({divLeft: increment(-1)})
          break;
        case Actions.Multiply:
          firebaseRequest.withPayload({multiLeft: increment(-1)})
          break;
        case Actions.Reset:
          firebaseRequest.withPayload({resetLeft: increment(-1)})
          break;
      }
      await firebaseRequest.executeWrite(true);
    }
  }

  async getHighScore() {
    const topUsers = await new FirebaseRequest().collection(Collections.USERS).orderBy('score', false).limit(10).execteRead()
    const ranks: Array<Ranks> = []
    topUsers.reverse().forEach(doc => {
      ranks.push({
        email: doc.data.email,
        points: doc.data.score,
        rank: ranks.length + 1,
        key: doc.data.email
      })
    })
    console.log(ranks)
      return ranks
  }

  async createNewUserProfile(email: string, id: string) {
    await new FirebaseRequest()
      .collection(Collections.USERS)
      .doc(id, "id")
      .withPayload({
        score: 0,
        email: email,
        divLeft: 5,
        multiLeft: 5,
        resetLeft: 1,
        tallys: [],
    }).executeWrite(false)
  }

  async checkBlackMarket(): Promise<Array<BlackMarketData>> {
    const blackMarketData: Array<BlackMarketData> = [];

    const BMdata = await new FirebaseRequest().collection(Collections.BLACK_MARKET).execteRead();
    const returnBMData: Array<BlackMarketData> = []

    BMdata?.forEach((Doc) => {
      const doc = Doc.data;
      if (doc.num > 0) {
        returnBMData.push({ key: doc.name, name: doc.name, numLeft: doc.num, price: 5 });
      }
    })

    return returnBMData;
  }

  async buyBlackMarketItem(itemKey: string) {
    await new FirebaseRequest()
      .collection(Collections.BLACK_MARKET)
      .doc(itemKey, 'id')
      .withPayload({num: increment(-1)});
  }

  private static getDomain(email: string) {
    return email.substring(email.indexOf('@'))
  }

  static getFirebaseHandler(): FirebaseHandler {
    if (!FirebaseHandler.firebaseHandler) {
      FirebaseHandler.firebaseHandler = new FirebaseHandler()
    }
    return FirebaseHandler.firebaseHandler
  }
}

export default FirebaseHandler
