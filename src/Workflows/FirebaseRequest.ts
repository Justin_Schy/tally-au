import { Collections } from "./FirebaseWorkflow";
import { collection, getFirestore, getDoc, getDocs, doc, DocumentData, QueryDocumentSnapshot, QuerySnapshot, query, where, updateDoc, addDoc, setDoc, orderBy, QueryConstraint, limit } from "firebase/firestore";
import FirebaseData from "../Models/FirebaseData";
import { documentId } from "firebase/firestore"
import Logger from "./Logger";

/**
 * Creates a new Firebase Request that can be used for either
 * reading or wrting, a collection must be set but docs and payloads are
 * optional depending on the type of request
 *
 * Read a collection (Collection)
 * Read a document (Collection, Document)
 * Create a new document in a collection (Collection, Payload)
 * Create a new document in a collection with a specific ID (Colelction, Document, Payload)
 * Update a document (Collection, Document, Payload)
 *
 * Limit and Order by are both optional
 * Limit defaults to 250
 * Order defailts to ''
 */
class FirebaseRequest {
  private _collection: string;
  private _doc: string
  private _payload: object | null;
  private _queryOperation: FieldOperation
  private queryConstraints: Array<QueryConstraint> = []
  private log = new Logger(this);

  constructor() {
    this._collection = "";
    this._doc = "";
    this._payload = null;
    this._queryOperation = FieldOperation.EQUAL;
  }

  /**
   * Sets the collection for the firebase request
   * REQUIRED
   * @param collectionRef - the collection you want to refrence
   */
  public collection(collectionRef: Collections): FirebaseRequest {
    this._collection = collectionRef;
    return this;
  }

  /**
   * Sets the document for the firebase request
   * @param documentId - the value the doc(s) contain
   * @param idField - the field you want to compare to, I.E. ID, or name
   * @param queryOperaion - the type of comparison
   */
  public doc(docId: string, idField: string, queryOperaion?: FieldOperation): FirebaseRequest {
    this._doc = docId;
    if (queryOperaion) this._queryOperation = queryOperaion;
    this.queryConstraints.push(where(idField === 'id' ? documentId() : idField, this._queryOperation, docId));
    return this;
  }

  /**
   * Adds a payload to the request
   * @param payload the updates to the doc, or the new doc contents.
   */
  public withPayload(payload: any): FirebaseRequest {
    this._payload = payload;
    return this;
  }

  public orderBy(orderField: string, ascending: boolean): FirebaseRequest {
    const orderDir = !ascending ? "desc" : undefined
    this.queryConstraints.push(orderBy(orderField, orderDir));
    console.log(this.queryConstraints)
    return this
  }

  public limit(num: number): FirebaseRequest {
    this.queryConstraints.push(limit(num));
    return this
  }

  /**
   * Creates a query for a firebase request
   * @param queryOperaion the type of operation
   * @private
   */
  private createFirebaseQuery(queryOperaion: FieldOperation) {
    if (!this._collection || !this._doc) {
      throw new Error("Attempt to create a firebase query without all feilds set, needs: collectiom, doc, and feild")
    }

    return query(collection(getFirestore(), this._collection), ...this.queryConstraints);
  }

  /**
   * Executes a write to the firebase database
   * @param update - true if the write is update
   * @param update - false if the write is create
   */
  public async executeWrite(update: boolean) {
    if (!this._payload || !this._collection) {
      this.log.error("No payload or collection set for executeWrite")
      return null;
    }

    /**
     * UPDATE A DOC
     */
    if (update) {
      if (!this._doc) {
        this.log.error("No document set for update in executeWrite")
        return null;
      }

      const docRef = doc(getFirestore(), this._collection, this._doc);
      updateDoc(docRef, this._payload);
      return this._doc;
    } else {
      /**
       * CREATE A DOC
       */

      // Create a doc with a set ID
      if (this._doc) {
        await setDoc(doc(getFirestore(), this._collection, this._doc), this._payload);
        return this._doc;
      }

      //Create a doc with a generated ID
      const docRef = await addDoc(collection(getFirestore(), this._collection), this._payload);
      return docRef.id
    }
  }

  public async execteRead() {
    if (!this._collection) {
      this.log.error("Collection was not set properly in executeRead")
    }

    const docs = this._doc ? await getDocs(this.createFirebaseQuery(this._queryOperation))
      : await getDocs(collection(getFirestore(), this._collection));
    const dataArray = FirebaseRequest.toFirebaseDataArray(docs, this._doc ? true : false);

    if (!dataArray) {
      throw new Error(`There were no docs found at ${this._collection}${this._doc ? '/' + this._doc : ''}`);
    } else {
      this.log.info(`Retrieved data from ${this._collection}${this._doc ? '/' + this._doc : ''}`);
      return dataArray;
    }


  }

  private static toFirebaseData(docData: QueryDocumentSnapshot<DocumentData>): FirebaseData {
    return {id: docData.id, data: docData.data()}
  }

  /**
   *
   * @param docData the doc data
   * @param singleDoc if the docData is a single doc
   */
  public static toFirebaseDataArray(docData: QuerySnapshot<DocumentData>, singleDoc: boolean) {
    const returnArray: Array<FirebaseData> = []
    if (docData.empty) {
      return null;
    }

    docData.forEach((doc) => {
      if (doc.exists()) {
        returnArray.push(this.toFirebaseData(doc));
      }
    })
    return returnArray;
  }

}

/**
 * CONTAINS ALL QUERY OPERATORS
 * https://firebase.google.com/docs/firestore/query-data/queries
 */
enum FieldOperation {
  LESS_THAN = "<",
  GREATER_THAN = ">",
  LESS_THAN_EQUAL = "<=",
  GREATHER_THAN_EQUAL = ">=",
  EQUAL = "==",
  NOT_EQUAL = "!=",
  ARRAY_CONTAINS = "array-contains",
  ARRAY_CONTAINS_ANY = "array-contains-any",
  IN = "in",
  NOT_IN = "not-in",
}


export default FirebaseRequest
export { FieldOperation }
