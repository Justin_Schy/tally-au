class Logger {
  private location: string

  constructor(location: object) {
    this.location = location.constructor.name
  }

  private getMessage(level: string, text: string) {
    return `[${level.toUpperCase()}] - ${this.location} \n\n ${text}`;
  }

  public info(message: string) {
    console.info(this.getMessage('info', message));
  }

  public status(message: string) {
    console.info(this.getMessage('status', message));
  }

  public debug(message: string) {
    console.debug(this.getMessage('debug', message));
  }

  public error(message: string) {
    console.error(this.getMessage('error', message));
  }
}

export default Logger;
