import { FirebaseError } from 'firebase/app';
import {
  collection,
  doc,
  getDoc,
  getDocs,
  getFirestore,
  increment,
  limit,
  onSnapshot,
  orderBy,
  query,
  updateDoc,
  DocumentData,
  arrayUnion,
  Timestamp,
  setDoc,
  Firestore
} from 'firebase/firestore'

import FirebaseRequest from "./FirebaseRequest";

class FirebaseWorkflow {

  private firestoreRef: Firestore;

  constructor() {
    this.firestoreRef = getFirestore();
  }
  public async readDocument(collectionRef: Collections, documentID: string): Promise<DocumentData | null> {
    try {
      const docSnap = await getDoc(doc(this.firestoreRef, collectionRef, documentID));

      if (docSnap.exists()) {
        return docSnap.data();
      } else {
        return null;
      }
    } catch(e: any) {
      console.error(`'/${collection}/${documentID}'`, e.message, e.code)
      return null;
    }
  }

  public async readAllDocuments(collectionRef: Collections): Promise <Array<DocumentData> | null> {
    try {
      const querySnapshot = await getDocs(collection(this.firestoreRef, collectionRef));
      const returnData: Array<DocumentData> = []

      const docData = FirebaseRequest.toFirebaseDataArray(querySnapshot, false);

      docData?.forEach((doc) => {
        returnData.push(doc.data);
      })

      return returnData;
    } catch (e: any) {
      console.error(`'/${collection}'`, e.message, e.code);
      return null;
    }
  }

  public async updateDocument(collection: string, documentID: string, payload: object) {
    console.log(collection, documentID, payload);
    const docRef = doc(this.firestoreRef, collection, documentID);

    await updateDoc(docRef, payload);
  }
}

enum Collections {
  TALLY = 'tally',
  USERS = 'users',
  BLACK_MARKET = 'blackMarket',
}

export default FirebaseWorkflow
export { Collections }
