import { Page } from '@playwright/test'
import LoginWorkflow from './LoginWorkflow'

class WorkflowHander {
  private static workflows: Array<workflow> = []
  private loginWorkflow: LoginWorkflow
  private page: Page

  constructor(page: Page) {
    this.page = page
  }

  getLoginWorkflow() {
    if (!this.loginWorkflow) {
      this.loginWorkflow = new LoginWorkflow(this.page)
    }
    return this.loginWorkflow
  }

  /**
   * Gets or creates a new workflow handler based on a page
   *
   * if a page is passed **recomended** then it will attempt to find a workflow associated
   * if the passed page does not have an associated workflow, then it will create a new one and append it
   *
   * if there is no page passed, then the most recently created workflow will be returned.
   * Please be careful if you are not passing the page, you very easily could have the wrong one.
   * @param page
   */
  static getOrCreateWorkflowHandler(page?: Page): WorkflowHander {
    if (page) {
      WorkflowHander.workflows.forEach(workflow => {
        if (workflow.page == page) return workflow
      })
      const workflowHandler = new WorkflowHander(page)
      WorkflowHander.workflows.push({ workflowHandler, page })
      return workflowHandler
    } else {
      return WorkflowHander.workflows[WorkflowHander.workflows.length - 1]
        .workflowHandler
    }
  }
}

interface workflow {
  workflowHandler: WorkflowHander
  page: Page
}

export default WorkflowHander
