import { Page } from '@playwright/test'

class DefaultWorkflow {
  page: Page

  constructor(page: Page) {
    this.page = page
  }

  protected locate(css: string) {
    return this.page.locator(css)
  }
}

export default DefaultWorkflow
