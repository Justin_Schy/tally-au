import { test, expect, Page } from '@playwright/test'
import defaultWorkflow from './defaultWorkflow'
import { getUserFromKey, getRandomUserKey } from '../users/Users'
import TestingConfig from '../testingConfig'

class LoginWorkflow extends defaultWorkflow {
  private static USERNAME_CSS = '#username'
  private static PASSWORD_CSS = '#password'
  private static SUBMIT_BUTTON_CSS = '#submit'

  constructor(page: Page) {
    super(page)
  }

  /**
   * Logins a specified user
   * Leave blank if you want a default user
   * @param userType - unused currently
   */
  private async login(username: string, password: string) {
    const loginButtonText = this.page.locator(
      `${LoginWorkflow.SUBMIT_BUTTON_CSS} > span`
    )
    await expect(loginButtonText).toHaveText('Log in')

    const usernameBox = this.locate(LoginWorkflow.USERNAME_CSS)
    const passwordBox = this.locate(LoginWorkflow.PASSWORD_CSS)
    const submit = this.locate(LoginWorkflow.SUBMIT_BUTTON_CSS)

    await usernameBox.type(username)
    await passwordBox.type(password)
    await submit.click()
  }

  async successLogin() {
    const user = getUserFromKey(getRandomUserKey())
    await this.login(user.email, user.password)

    const currentTally = this.locate('.ant-menu-title-content >> nth=0')
    await expect(currentTally).toHaveText('Current Tally')
  }

  async failLogin() {
    await this.login('jkschy1@gmail.com', 'test1234')

    const notification = this.locate('.ant-notification-notice-message')
    await expect(notification).toHaveText('Sign in failed.')
  }

  async goToHomepage() {
    await this.page.goto(TestingConfig.default_url)
  }
}

export default LoginWorkflow
