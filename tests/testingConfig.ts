interface testingConfig {
  default_url: string
}

const testingConfig: testingConfig = {
  default_url: 'localhost:5000',
}

export default testingConfig
