const users: Array<user> = [
  {
    key: '1',
    email: 'test@anderson.edu',
    password: 'test1234',
  },
]

/**
 * the interface of a user
 *
 * role is currently unused as I dont have any other roles
 */
interface user {
  key: string
  email: string
  password: string
  role?: string
}

/**
 * gets a random user key
 *
 * will use roll eventually, but not right now. noted above.
 */
function getRandomUserKey(role?: string): string {
  return users[0].key
}

function getUserFromKey(key: string): user {
  const filtUsers = users.filter(user => user.key === key)
  if (filtUsers.length === 1) {
    return filtUsers[0]
  } else {
    throw new Error(`There is more then one user with the key ${key}`)
  }
}

export { getRandomUserKey, getUserFromKey }
