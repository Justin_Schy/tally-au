import { test, expect } from '@playwright/test'
import WorkflowHandler from '../Workflows/WorkflowHander'

test.beforeEach(async ({ page }) => {
  new WorkflowHandler(page).getLoginWorkflow().goToHomepage()
})

test('Successfully login', async ({ page }) => {
  const loginWorkflow =
    WorkflowHandler.getOrCreateWorkflowHandler(page).getLoginWorkflow()
  await loginWorkflow.successLogin()
})

test('Fails to login', async ({ page }) => {
  const loginWorkflow =
    WorkflowHandler.getOrCreateWorkflowHandler(page).getLoginWorkflow()
  await loginWorkflow.failLogin()
})
