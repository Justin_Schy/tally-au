Current Status: <br>
[![Netlify Status](https://api.netlify.com/api/v1/badges/3b54e5b6-7b18-4b32-877c-f782f3d08481/deploy-status)](https://app.netlify.com/sites/tallyau/deploys)

#Welcome to TallyAu
TallyAu is a game created in the likeness of `r/TheButton` from
reddit. This means that there is really no reason to play except for
the fun of the game.

##Run Locally
To Run TallyAu Locally follow these steps

1. Clone Repo
2. `npm install`
3. `npm run start`
4. go to `localhost:3000` (Step 3 should do this automatically)

##Notes

- Accounts must be anderson.edu email addresses
- Currently, works with my personal firebase. So don't use it much please.
